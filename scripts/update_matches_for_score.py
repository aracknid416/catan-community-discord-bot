import json
import urllib
import asyncio

import motor.motor_asyncio


async def generate_data(config):
    cluster = motor.motor_asyncio.AsyncIOMotorClient(
        f'mongodb+srv://CCOpenDev:{urllib.parse.quote_plus(config["db"]["password"])}'
        f'@ccdevcluster0.ndot3.mongodb.net/CCDevDB0?retryWrites=true&w=majority')

    if not config["db"].get("database") or True:
        db = cluster.get_default_database()
    else:
        db = cluster[config["db"]["database"]]

    users = db["users"].find()
    async for user in users:
        print("processing " + user.get("display_name", user["discord_username"]))
        matches = db["matches"].find(
            {
                "players": user["discord_username"]
            }
        )
        nrof_wins = 0
        nrof_matches = 0
        async for match in matches:
            if match["winner"] == user["discord_username"]:
                nrof_wins += len(match["players"])
            nrof_matches += 1

        await db["users"].update_one(
            {
                "_id": user["_id"]
            },
            {
                "$set": {
                    "matches": nrof_matches,
                    "normalized_wins": nrof_wins
                }
            }
        )


if __name__ == "__main__":
    with open('../secret.json') as f:
        config = json.load(f)

    asyncio.run(
        generate_data(config)
    )
