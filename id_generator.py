import random
import re
import string

colors = [
    "Red",
    "Green",
    "Blue",
    "Orange",
    "Yellow",
    "Black",
    "Pale",
    "Purple",
    "Brown",
    "Azure",
    "Ruby",
    "Chartreuse",
    "Silver",
    "Gold",
    "Copper",
    "Tin",
    "Led",
    "Ice",
    "Iron",
    "Steel",
    "Tungsten",
    "Wooden",
    "Digital",
    "Cardboard",
    "Paper",
    "Alien",
    "Ceramic",
    "Obtuse",
    "Diagonal",
    "Foam",
    "Cold",
    "Lukewarm",
]

items = [
    "Road",
    "Village",
    "City",
    "Point",
    "Tile",
    "Wheat",
    "Brick",
    "Wool",
    "Lumber",
    "Ore",
    "Robber",
    "Knight",
    "Monopoly",
    "Army",
    "Gold",
    "Trophy",
    "Pirate",
    "Ship",
    "Hex",
    "Empire",
    "Dice",
    "Number",
    "Meeple"
]


def generate_id():
    return random.choice(colors) + random.choice(items) + str(random.randint(1, 999)) + \
           random.choice(string.ascii_uppercase)


expr = re.compile(
    '^(' + '|'.join(re.escape(i) for i in colors) + ')(' + '|'.join(re.escape(i) for i in items) + r")\d{1,3}[A-Z]$"
)


def match(msg):
    return expr.match(msg)


if __name__ == "__main__":
    print(len(colors), len(items))
    print(len(colors) * len(items) * 1000 * len(string.ascii_uppercase))
    print((len(colors) * len(items) * 1000 * len(string.ascii_lowercase)) ** 0.5)
