import json
import asyncio

import discord

ready_count = 0

BOT_USERNAME = "CatanCommunityBotDev"


def extract_game_id(content):
    words = content.split()
    id_index = words.index("id") + 1
    return words[id_index][1:-1]


async def get_last_message(channel) -> discord.Message:
    async for message in channel.history(limit=1):
        return message


async def on_ready():
    global ready_count
    ready_count += 1
    print("incrementing ready count: " + str(ready_count))
    if ready_count < 2:
        return

    channel1 = clients[0].guilds[0].system_channel
    channel2 = clients[1].guilds[0].system_channel

    await channel1.send("!creategame 25")

    await asyncio.sleep(0.5)

    match_message = await get_last_message(channel2)
    assert match_message.author.name == BOT_USERNAME

    await match_message.add_reaction('🎮')

    game_id = extract_game_id(match_message.content)

    await asyncio.sleep(0.5)

    with open("example_image.png", 'rb') as f:
        await channel2.send("!winner " + game_id + " " + clients[1].user.mention, files=[discord.File(f)])

    await asyncio.sleep(0.5)

    dispute_message = await get_last_message(channel1)
    assert dispute_message.author.name == BOT_USERNAME
    await dispute_message.add_reaction('👍')

    await asyncio.sleep(0.5)
    await channel1.send("!rank")
    await channel2.send("!rank")
    await asyncio.sleep(0.25)
    await channel2.send("!leaderboard")


if __name__ == "__main__":
    with open("secret.json") as f:
        config = json.load(f)

    clients = [discord.Client() for i in config["testing_tokens"]]
    for client in clients:
        client.event(on_ready)

    # for client in clients:
    # client.start()
    try:
        asyncio.get_event_loop().run_until_complete(
            asyncio.gather(*(
                i.start(token) for i, token in zip(clients, config["testing_tokens"]))))
    finally:
        asyncio.get_event_loop().close()
