import discord
import json

client = discord.Client(intents=discord.Intents.all())


@client.event
async def on_reaction_add(reaction, user):
    print("on_reaction_add called")


@client.event
async def on_reaction_remove(reaction, user):
    print("on_reaction_remove called")


# same problem if I remove this part
#@client.event
#async def on_raw_reaction_remove(payload):
#    print("on_raw_reaction_remove called")


with open("secret.json") as f:
    config = json.load(f)
token = config["token"]
client.run(token)
