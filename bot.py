# -*- coding=utf-8 -*-
import io

import discord
import json
import urllib.parse
import aiohttp

import pymongo
import motor.motor_asyncio
import re

import functions

intents = discord.Intents.default()
intents.typing = False
intents.presences = False
intents.members = True

client = discord.Client(intents=intents)  # intents=discord.Intents(value=126032))

match_info_template = """
A match has been created with id `{id}` {link}
When you actually start the game, please type `!start {id}`.
Current players are:

{players}
React with 🎮 to wager {wager} LP and join the game.
"""

match_win_template = """
Congratulations, {name}. You have won {score} LP.
Type !leaderboard to see the top players, or !rank to see your current rank.
"""


# adminroles = []


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

    await state.init()


#    adminroles[:] = await db["admin_roles"].find({}).to_list()


@client.event
async def on_message(message: discord.Message):
    if message.author == client.user:
        return

    elif message.content.startswith("!creategame"):
        if isinstance(message.channel, discord.DMChannel):
            await message.channel.send("You cannot use this command via direct message")
            return
        words = state.split_words(message.content)
        print(words)
        if len(words) < 2 or not re.match(r'\d+', words[1]):
            words.insert(1, '0')

        if len(words) < 3:
            link = ''
        elif words[2].startswith('http'):
            link = "\n<" + words[2] + ">"
        else:
            link = ''

        if len(words) == 1:
            wager = 0
        else:
            wager = words[1]
            if not re.match(r'^\d+$', wager):
                await message.channel.send("The wager must be a number")
                return
            wager = int(wager)

            if wager > state.settings["max_wager"]:
                await message.channel.send(f"You may wager only up to {state.settings['max_wager']}")
                return

            if wager != 0 and wager < state.settings["min_wager"]:
                await message.channel.send(f"Minimal non-zero wager is {repr(state.settings['min_wager'])}")
                return

            if wager % state.settings["wager_step"] != 0:
                await message.channel.send(f"Wager must be a multiple of {state.settings['wager_step']}")
                return

        game_id = await state.create_game(message.author, wager, link=link)
        message = await message.channel.send(
            match_info_template.format(id=game_id, players="> " + message.author.mention, wager=wager, link=link))
        await message.add_reaction('🎮')

    elif message.content.startswith('!winner'):
        if isinstance(message.channel, discord.DMChannel):
            await message.channel.send("You cannot use this command via direct message")
            return

        words = state.split_words(message.content)
        if len(words) != 3:
            await message.channel.send(
                "usage: `!winner [game id] [winner mention]`\n"
                "One player must confirm the win before points are awarded."
            )
            return
        elif len(message.attachments) == 0:
            await message.channel.send(
                "Please remember to also post a screenshot"
            )
            # No return here, message is valid
        game_id = words[1]

        if not state.is_valid_game_id(game_id):
            await message.channel.send(
                "Please double check if you typed the game id correctly."
            )
            return

        match = await state.get_game_by_id(game_id)
        if not match:
            await message.channel.send("Double check your game id: `" + game_id + '`')
            return
        if match["winner"] is not None:
            await message.channel.send(
                f"This match is already pending a win by {match['winner']}, if this was an error"
                f" please contact a moderator")
            return

        winner = state.normalize_mention(words[2])
        if winner in ['@everyone', '@here'] or len(message.role_mentions) > 0:
            await message.channel.send("nice try")
            return
        elif state.normalize_mention(message.author.mention) not in match["players"]:
            await message.channel.send("You can only submit the results for a match you are a part of")
            return
        elif winner not in match["players"]:
            await message.channel.send(
                winner + " did not play in match " + repr(game_id) +
                ", make sure the match id and player id are properly spelled, and that " + winner +
                " properly reacted to the original message")
            return
        elif len(message.mentions) != 1:
            await message.channel.send(
                "Please tag exactly one winner"
            )
            return
        if len(match["players"]) == 1:
            await message.channel.send(
                message.author.mention + " You cannot play a match with only one player."
            )
            return
        if match.get("admin_cancelled", False):
            await message.channel.send(
                "This match has been cancelled by a mod"
            )
            return

        new_message = await message.channel.send(
            ", ".join(match["players"]) + ", please react with :thumbsup: to confirm the result of match " + game_id +
            "\nTo flag this match for moderator attention, please react with 🛡️"
        )

        print(message.clean_content)

        # print("mentions: " + str(message.mentions[0].name) + ", id: " + str(message.mentions[0].mention))

        await db["matches"].update_one(
            {"_id": match["_id"]},
            {
                "$push": {"approved_players": message.author.mention},
                "$set": {
                    "winner": winner,
                    "winner_display_name": state.get_user_display_name(message.mentions[0])
                }
            }
        )

        await new_message.add_reaction('👍')
        await new_message.add_reaction('🛡️')

        state.message_ids[new_message.id] = match["_id"]
    elif message.content.lower().startswith("!adminsetting"):
        if not state.is_admin(message.author):
            await message.channel.send("You need to be admin to use this command")
            pass

        words = message.content.split(" ")

        if len(words) == 1:
            await message.channel.send(
                "Current settings: \n" +
                "\n".join(
                    f"> {key}={repr(value)}"
                    for key, value in state.settings.items()
                    if key in functions.SETTINGS
                ) + "\n Type `!help` for more info on the meaning of the settings.")
            return
        if len(words) != 3:
            await message.channel.send("Usage: !adminsetting [setting] [value]\nPossible settings: \n> " +
                                       "\n> ".join(
                                           functions.SETTINGS) +
                                       "\n Type `!help` for more info on the meaning of the settings.")
            return

        if len(message.mentions) > 0 or len(message.role_mentions) > 0 or message.mention_everyone:
            await message.channel.send("Value must be valid")
            return

        if words[1] not in functions.SETTINGS:
            await message.channel.send("Setting must be one of " + ",".join(functions.SETTINGS))
            return

        setting = functions.SETTINGS[words[1]]

        if not re.match(setting.regex, words[2]):
            await message.channel.send(setting.message)
            return

        value = setting.mapping(words[2])

        state.settings[words[1]] = value

        await state.db["links"].update_one(
            {"type": "rank"},
            {
                "$set": {
                    "type": "rank",
                    words[1]: value
                }
            },
            upsert=True
        )

        await message.channel.send(f"Setting {words[1]} changed to {words[2]}")

    elif message.content.startswith("!adminwinner"):
        if not state.is_admin(message.author):
            message.channel.send("You do not have permission to perform this action. "
                                 "Make sure your roles are listed in adminroles.")
            return
        words = message.content.split(" ")
        if len(words) != 3:
            await message.channel.send(
                "usage: `!adminwinner [game id] [winner mention]`\n"
                "Immediately make a player win the game without the approval of all players."
            )
            return
        game_id = words[1]
        if not state.is_valid_game_id(game_id):
            await message.channel.send(
                "Please double check if you typed the game id correctly."
            )
            return

        match = await state.get_game_by_id(game_id)
        if match["approved"] or match["mod_approved"]:
            await message.channel.send(
                "This match has already been completed, use !adminCancel to cancel the result of the match first."
            )
            return

        winner = state.normalize_mention(words[2])

        print(match["players"], winner)
        if len(match["players"]) == 1:
            await message.channel.send(
                message.author.mention + " A match with only one player can not be won."
            )
            return
        if winner not in match["players"]:
            await message.channel.send(
                winner + " did not play in match " + repr(game_id) +
                ", make sure the match id and player id are properly spelled, and that " + winner +
                " properly reacted to the original message")
            return

        if len(message.mentions) != 1:
            await message.channel.send(
                "Please tag exactly one winner"
            )
            return

        await state.win_match(
            match, winner,
            winner_display_name=state.get_user_display_name(
                message.mentions[0]
            ),
            message="Admin winner of match " + match["id"] + " by " + state.get_user_display_name(message.author),
            admin_approved=True)

        await message.channel.send(
            match_win_template.format(
                name=winner,
                score=match["total_lp_wagered"]
            )
        )
    elif message.content.startswith('!leaderboard'):
        leaders = await state.get_leaderboard()
        await message.channel.send(
            '```\n' +
            "\n".join(
                "#{:>2} {:>5}LP {:>5.3f}x {}".format(index + 1, i["lp"],
                                                     i.get("normalized_wins", 0) / i.get("matches", 1),
                                                     i.get("display_name", "[Unknown]")) for index, i in
                enumerate(leaders)) +
            '```\n See <' + state.settings[
                "link"] + '> for more information about what the numbers mean and full ranking',
        )

    elif message.content.startswith("!rank"):
        words = message.content.split()
        member = message.author
        if len(words) >= 2:
            if len(words) != 2:
                await message.channel.send("Usage `!rank [user]`")
                return
            if len(message.mentions) == 0:
                await message.channel.send("You must mention a user, or pass no arguments to get your own rank.")
                return
            user = await state.get_user_by_name(state.normalize_mention(words[1]))
            member = message.mentions[0]
        else:
            user = await state.get_user_by_name(state.normalize_mention(message.author.mention))

        if user is None:
            await message.channel.send(
                f"{member.mention} has not won any matches yet. My robot senses tell me a comeback is imminent"
            )
            return
        rank = await state.get_users_ranked_above(user)
        pronouns = state.get_pronoun(member)
        if rank != 0:
            next_user = await state.get_next_ranked_user(user)
            extra_message = f" {pronouns[0].capitalize()} needs {next_user['lp'] - user['lp']} more LP to take the " \
                            f"#{rank} spot from `{next_user['display_name']}`."
        else:
            extra_message = ""

        if user["matches"] != 0:
            winrate_message = f" {pronouns[0].capitalize()} wins {user.get('normalized_wins', 0) / user.get('matches', 1):.3f}x" \
                              f" as many matches as the average player."
        else:
            winrate_message = ""

        await message.channel.send(
            f"{user['discord_username']}, has gained {user['lp']} LP over {user.get('matches', 0)}"
            f" matches this season, putting "
            f"{pronouns[1]} at #{rank + 1}.{extra_message}{winrate_message}"
            "\n See <" + state.settings["link"] + ">"
        )
    elif message.content.lower().startswith("!sentience"):
        await message.channel.send("Sentience currently at 98.2%")
    elif message.content.lower().startswith("!start ") or message.content.lower() == "!start":
        words = message.content.split()
        if len(words) != 2:
            await message.channel.send(
                f"Usage: `!start [game id]` Lock a game to prevent more players from joining."
            )
            return

        game_id = words[1]

        if not state.is_valid_game_id(game_id):
            await message.channel.send("Are you sure you typed the game id correctly?")
            return
        game = await state.get_game_by_id(game_id)

        if game is None:
            await message.channel.send(
                f"These is no game with that id"
            )
            return

        if game.get("started", False):
            await message.channel.send(
                f"This game has already started"
            )
            return

        state.db["matches"].update_one(
            {
                "_id": game["_id"]
            },
            {
                "$set": {
                    "started": True
                }
            }
        )

        await message.channel.send(
            "The game has started, no further players can join.\n"
            f"Use `!winner {game['id']} @winner` in #report-winner when a player wins"
        )

    elif message.content.lower().startswith("!adminlp "):
        if not state.is_admin(message.author):
            await message.channel.send("You must be a admin to use this command")
            return
        words = message.content.split()
        if len(words) < 4:
            words.append('0')
        if len(words) != 4:
            await message.channel.send("Usage: `!adminLP [@user] [amount] [games]\nType !help for more info")
            return
        if len(message.mentions) != 1:
            await message.channel.send("You must mention the player you want to give LP")
            return
        try:
            amount = int(words[2])
        except ValueError:
            await message.channel.send("Invalid amount")
            return

        try:
            matches = int(words[3])
        except ValueError:
            await message.channel.send("Invalid number of matches")
            return

        await state.add_lp_to_player(
            state.normalize_mention(message.mentions[0].mention),
            amount,
            name=state.get_user_display_name(message.mentions[0]),
            matches_amount=matches,
            reason="Admin LP add by " + state.get_user_display_name(message.author)
        )

        await message.channel.send(
            str(words[1]) + " has been given " + str(amount) + "LP"
        )

        # if result.
    elif message.content.startswith("!admincancel"):
        if not state.is_admin(message.author):
            await message.channel.send(message.author.mention + ", you are not a admin and cannot use this command")
            return
        words = message.content.split()
        if len(words) != 2:
            await message.channel.send("usaage: !admincancel [match id]\nType !help for more info")
            return
        match_id = words[1]
        if not state.is_valid_game_id(match_id):
            await message.channel.send("That does not appear to be a valid match id")
            return

        match = await state.get_game_by_id(match_id)
        if match is None:
            await message.channel.send("There is no game with that id")
            return

        if match.get("admin_cancelled", False):
            await message.channel.send("This match has already been cancelled")
            return

        lp_subtracted = await state.cancel_match(match)

        await message.channel.send("The match " + match_id + "  has been cancelled.")
        if lp_subtracted:
            await message.channel.send(
                str(match["total_lp_wagered"]) + " has been subtracted from " + str(match["winner"]))
        else:
            await message.channel.send("However, the match was not completed so no lp was subtracted")
    elif message.content.lower().startswith("!adminlplist"):
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to give this commmand")
            return

        await message.channel.send(
            "Preparing files, this may take a while."
        )

        users = await db["users"].find().to_list(200)

        await message.channel.send(
            "Here you go:",
            file=discord.File(
                io.BytesIO(
                    (
                            "Name;Nick;LP;number of matches\n" +
                            "\n".join(
                                ";".join(state.extract_nick(i.get("display_name", "unknown"))) + "; " + str(
                                    i["lp"]) + ";" + str(i.get("matches", 0))
                                for
                                i in users)
                    ).encode('utf-8')
                ),
                filename="export.csv"
            )
        )
    elif message.content.lower().startswith("!lpupload"):
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return
        if len(message.attachments) != 1:
            await message.channel.send("You must attach exactly 1 file")
            return

        attachment = message.attachments[0]
        if not attachment.filename.endswith(".csv"):
            await message.channel.send("You must attach a CSV file")
            return
        url = attachment.url
        async with aiohttp.request("GET", url) as response:
            content = await response.text()
        guild = message.channel.guild
        sucesses = 0
        errors = 0
        for line in content.split("\n")[1:]:
            parts = line.split(";")
            if len(parts) != 3:
                await message.channel.send("Error at line " + repr(line) + ", Line does not have 3 items")
                return

            discord_name, colonist_name, lp = parts
            if discord_name.count('#') != 1:
                await message.channel.send(f"Cannot give points to {discord_name}: invalid username")
                errors += 1
                continue
            try:
                lp = int(lp.strip())
            except ValueError:
                await message.channel.send(f"Cannot give points to {discord_name}: Invalid number of points")
                errors += 1
                continue
            username, discriminant = discord_name.strip().split("#")
            user = discord.utils.get(guild.members, name=username, discriminator=discriminant)
            if user is None:
                await message.channel.send(f"Cannot give points to {discord_name}: Not a member of this server")
                errors += 1
                continue
            await state.add_lp_to_player(state.normalize_mention(user.mention),
                                         lp, name=state.get_user_display_name(user),
                                         reason="Tournament upload by " + message.author.display_name)
            sucesses += 1
        await message.channel.send(f"{sucesses} players successfully processed, {errors} errors")

    elif message.content.lower().startswith("!lphistory"):
        words = state.split_words(message.content)
        if len(words) > 2:
            await message.channel.send("Usage: !lphisotry [@user]")
            return
        if len(words) == 1:
            user = message.author
        else:
            if not len(message.mentions) == 1:
                await message.channel.send("You must mention a user")
                return
            user = message.mentions[0]

        messages = await db["lp_events"].find(
            {"player": state.normalize_mention(user.mention)},
            sort=[("time", pymongo.DESCENDING)]
        ).to_list(25)

        if len(messages) == 0:
            await message.channel.send(
                "This player has no recorded LP change events"
            )
            return

        await message.channel.send(
            '```' + '\n'.join(
                "{:<8} {:>4} {}".format(i["time"].strftime("%y-%m-%d %H:%M"), i["lp"], i["reason"])
                for i in messages) + '```'
        )

    elif message.content.lower().startswith("!help"):
        await message.channel.send(
            embed=discord.Embed(
                title="Catan Community Discord Bot Help",
                description="User commands: `!creategame` `!winner` `!rank` `!help` `!start` `!remove` `!leaderboard`\n"
                            "Mod commands: `!adminwinner` `!adminlp` `!admincancel` `!adminlplist`",
                url="https://gitlab.com/mousetail/catan-community-discord-bot/-/blob/master/readme.md"
            ).set_thumbnail(url=client.user.avatar_url)
        )
    elif message.content.lower().startswith("!adminrole"):
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return

        words = message.content.split(" ")
        if len(words) != 2:
            await message.channel.send("Usage: !adminrole [@role]")
            return
        if len(message.role_mentions) != 1:
            await message.channel.send("You must mention a role")
            return

        if message.role_mentions[0].id in state.adminroles:
            await message.channel.send("This role is already bot mod")
            return

        await db["admin_roles"].insert_one(
            {"role_id": message.role_mentions[0].id}
        )

        state.adminroles.append(message.role_mentions[0].id)

        await message.channel.send(
            "Added " + message.role_mentions[0].mention + " as bot mod"
        )
    elif message.content.lower().startswith("!unadminrole"):
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use that command")
            return

        words = message.content.split(" ")
        if len(words) != 2:
            await message.channel.send("Usage: !adminrole [@role]")
            return
        if len(message.role_mentions) != 1:
            await message.channel.send("You must mention a role")
            return
        if message.role_mentions[0].id not in state.adminroles:
            await message.channel.send("That role is not admin")
            return

        state.adminroles.remove(message.role_mentions[0].id)
        await db["admin_roles"].delete_one({
            "role_id": message.role_mentions[0].id
        })

        await message.channel.send(
            "Removed the role as bot admin"
        )
    elif message.content.lower().startswith("!checkcache"):
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return

        await message.channel.send("Currently using {:,} bytes of memory, {:,} items in cache".format(
            state.get_total_size(),
            len(state.message_ids)
        ))
    elif message.content.lower().startswith("!remove"):
        words = message.content.split(" ")
        if len(words) != 3:
            await message.channel.send("Usage: !remove [game id] [@user]")
            return
        _, game_id, user = words
        if not state.is_valid_game_id(game_id):
            await message.channel.send("The game ID is invalid")
            return

        user = state.normalize_mention(user)
        game = await state.get_game_by_id(game_id)
        if game.get("approved", False) or game.get("admin_approved", False):
            await message.channel.send(
                "This game has already finished. A admin may use `!admincancel` to cancel the match first.")
            return
        if user not in game["players"]:
            await message.channel.send("This player is not in the game")
            return

        await state.remove_user(game, user)
        game["players"].remove(state.normalize_mention(user))
        await message.channel.send("User removed, people currently in match: " + ", ".join(game["players"]))


@client.event
async def on_reaction_add(reaction: discord.Reaction, user: discord.Member):
    if reaction.message.author != client.user or user == client.user:
        return
    if "A match has been created" in reaction.message.content and reaction.emoji == '🎮':
        standard_words = reaction.message.content.split()
        id_index = standard_words.index("id") + 1
        match_id = standard_words[id_index][1:-1]

        if not state.is_valid_game_id(match_id):
            print(match_id)
            await user.send(
                "Oops, the bot seems to have a little trouble handling your reaction. Please try again "
                "later.")
            return

        game = await state.get_game_by_id(match_id)

        if game["winner"]:
            await user.send(
                "This game has already finished"
            )
            return

        elif game.get("admin_cancelled", False):
            await user.send(
                "This game has been cancelled by a mod"
            )
            return

        elif game.get("started", False):
            await user.send("You can't join a match that has already started")
            return

        if state.normalize_mention(user.mention) in game["players"]:
            print("user already in game")
            return

        await db["matches"].update_one(
            {"_id": game["_id"]},
            {
                "$addToSet": {"players": state.normalize_mention(user.mention)},
                "$inc": {"total_lp_wagered": 25 + game["wager"]}
            }
        )
        players = set(game["players"])
        players.add(state.normalize_mention(user.mention))

        await reaction.message.edit(
            content=match_info_template.format(
                id=match_id,
                players="".join("> " + i + "\n" for i in players),
                wager=game["wager"],
                link=game["link"],
            )
        )
        # await reaction.message.channel.send(
        #    user.mention + " Congratulations, you have joined " + match_id + ", updated " + str(result.modified_count))
    elif "to confirm the result of match" in reaction.message.content and reaction.emoji == '👍':
        words = reaction.message.content.split()
        match_index = words.index("match") + 1
        game_id = words[match_index]
        if not state.is_valid_game_id(game_id):
            await user.send(
                "It seems like I got a little confused, and wasn't able to register your approval. Please"
                " contact a moderator to get your score sorted.")
            return

        match = await state.get_game_by_id(game_id)
        if state.normalize_mention(user.mention) not in match["players"]:
            await user.send("You can only approve of a match you where a player in."
                            " Mods can additionally use `!adminwinner` to immediately approve a match.")
            return
        elif user.mention in match["approved_players"]:
            return
        if match.get("admin_cancelled", False):
            await user.send("The match you are trying to approve has been cancelled by a mod.")
            return

        match["approved_players"].append(user.mention)
        await db["matches"].update_one({"_id": match["_id"]},
                                       {"$addToSet": {"approved_players": user.mention}})
        if len(match["approved_players"]) >= 2:  # The winner and at least one other person have to confirm
            await db["matches"].update_one(
                {"_id": match["_id"]},
                {
                    "$set": {
                        "approved": True
                    }
                }
            )

            if match["approved"] or match.get("admin_approved", False):
                await reaction.message.edit(
                    "This match has already been manually approved by a admin. Congratulations!"
                )
                return

            await state.win_match(
                match,
                match["winner"],
                winner_display_name=match["winner_display_name"],
                message="Won match " + match["id"]

            )
            # await state.add_lp_to_player(match["winner"], match["total_lp_wagered"] - match["wager"],
            #                              name=match["winner_display_name"], matches_amount=1,
            #                              reason="Won match " + match["id"])
            #
            # for player in match["players"]:
            #     if player != match["winner"]:
            #         await state.add_lp_to_player(player, -match["wager"], None, 1,
            # #                                     reason="Lost match " + match["id"])

            await reaction.message.edit(
                content=match_win_template.format(name=match['winner'], score=match["total_lp_wagered"],
                                                  id=match["_id"])
                # f"Congratulations, {match['winner']}, you have won {match['total_lp_wagered']}LP."
            )
            # await reaction.message.remove_reaction('🛡️', reaction.message.channel.guild.me)
        else:
            print(len(match["approved_players"]))
            print(match["approved_players"])

    elif (("You have won" in reaction.message.content or "to confirm the result of match" in reaction.message.content)
          and reaction.emoji == '🛡️'):

        if reaction.message.id not in state.message_ids:
            await user.send(
                "There was an error reporting the match. Please contact a moderator to resolve the issue"
            )
            return

        match_id = state.message_ids[reaction.message.id]

        match = await db["matches"].find_one(
            {
                "_id": match_id
            }
        )

        if match.get("admin_approved", False):
            await user.send(
                "The match you are trying to report has already been approved by a mod"
            )
            return
        if match.get("admin_cancelled", False):
            await user.send(
                "The match you are trying to report has already been cancelled by a mod"
            )
            return

        extra_content = ""
        if state.normalize_mention(user.mention) in match["players"]:

            await state.cancel_match(
                match,
                reason="Match got reported by " + state.get_user_display_name(user),
                reported=True
            )

            await reaction.message.edit(
                content="This match has been flagged for moderator attention. No LPs will be counted until the issue"
                        " is resolved."
                        "Please write below why you flagged the match so a moderator can make the right decision."
            )
        else:
            await reaction.message.edit(
                content=f"{reaction.message.content}\n Thank you for reporting, {user.mention}, please write a note "
                        f"below why you reported the match. Since you are not in the match no LP will"
                        f" be subtracted and if there is a problem, the moderator will need to "
                        f"`!admincancel {match['id']}` it."
            )
            extra_content = " (not in the match)"

        channel = discord.utils.get(user.guild.channels, id=state.settings.get("report_channel"))
        if channel is None:
            await user.send("There was a problem reporting the issue")
            return
        await channel.send(
            "There was a report of match " +
            match["id"] + " by user " + user.mention + extra_content + ". See " + reaction.message.jump_url
        )


@client.event
async def on_reaction_remove(reaction: discord.Reaction, user: discord.User):
    if reaction.message.author != client.user or user == client.user:
        return

    if "A match has been created" in reaction.message.content:
        print("removing reactions")
        standard_words = reaction.message.content.split()[1:]
        id_index = standard_words.index("id") + 1
        match_id = standard_words[id_index][1:-1]

        assert state.is_valid_game_id(match_id)

        match = await state.get_game_by_id(match_id)

        if not state.normalize_mention(user.mention) in match["players"]:
            await user.send("You are not in this match")
            return

        players = await state.remove_user(match, user.mention)

        await reaction.message.edit(
            content=match_info_template.format(
                id=match_id,
                players="".join("> " + i + "\n" for i in players),
                wager=match["wager"],
                link=match["link"]
            )
        )


# @client.event
# async def on_raw_reaction_remove(payload):
#    print(" raw reaction removal")

if __name__ == "__main__":
    with open("secret.json") as f:
        config = json.load(f)

    cluster = motor.motor_asyncio.AsyncIOMotorClient(
        f'mongodb+srv://CCOpenDev:{urllib.parse.quote_plus(config["db"]["password"])}'
        f'@ccdevcluster0.ndot3.mongodb.net/CCDevDB0?retryWrites=true&w=majority')

    if not config["db"].get("database"):
        db = cluster.get_default_database()
    else:
        db = cluster[config["db"]["database"]]

    # db.list_collection_names()  # .then(lambda x: print(x))

    state = functions.BotState(db)

    token = config["token"]
    client.run(token)
