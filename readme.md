# Basic workflow

Start a game with `!creategame`, wait for people to join.

When you start, type `!start` so lock the users for the game.

When the game is over, type `!winner` to declare a winner.

# GP Bot Commands

Commands tagged with ✈️ can be sent over direct message.

Commands tagged with 🛡️ can only be used by moderators

`!creategame [wager]` Make a game. React to the result to join a game. Joining a game will add 25 lp to the prize pool. Wager is optional.

`!winner [game id] [@winner]` Finish a game. This message must include a screenshot of the final scores. All players must
react to the message to confirm.

`!rank` ✈️ Shows your rank and total lp.

`!rank [@user]` get a specific users rank

`!lphistory` ✈️ get your most recent LP events

`!lphistry [@user]` Get a users LP history

`!leaderboard` ✈️ Shows the 25 best players.

`!help` ✈️Shows this page.

`!start [game id]` Mark a game as started, so that no further players can join.

`!remove [game id] [user mention]` Remove a user from a game

# Mod Commands

These commands require the user to be admin/mod.

`!adminwinner [game id] [@winner]` 🛡️ Immediately closes a match and awards the specified winner VP.

`!adminlp [@user] lp` 🛡️ Gives \@user lp according to `lp`. You can use `!adminlp [@user] 0` to add a user to the
leaderboard without giving them any lp.

`!admincancel [game id]` 🛡️ Cancels a game. If the game is finished refund lp. If not, set a flag that prevents
if from being `!winner` from working.

`!adminlplist` 🛡️✈️ Gets all players LP's as a CSV file. Guaranteed to work upto 500 players.

`!lpupload` 🛡️✈️ Adds LP to all players based on a CSV file. File should be ; seperated and have 3 columns:
Discord name, (ignored), LP

`!adminsetting` 🛡️✈️ Sets game settings:

Options for settings:

| Setting | Meaning | 
| :----   | :------ |
| link    | The link spammed in `!rank` and `!leaderboard` commands. |
| min_wager | The minimal wager amount allowed, 0 wagers are always allowed |
| max_wager | The maximum wager allowed per person per match |
| wager_step | The wager must be a multiple of `wager_step`. Can be set to 1 to disable the step. |
| report_channel | The ID for the channel reports should be sent to. |


# Mod Management Commands

`!adminrole [@role]` 🛡️✈️ Adds `role` to the list of roles permitted to use admin commands.

`!unadminrole [@role]` 🛡️✈️ Removes `role` form the list of roles permitted to use admin commands. The server owner
can always use admin commands regardless of the role settings. 

# Contributing

I am just one guy and also have a full time job, so won't always be able to fix all the
issues with the bot. If you find a problem and don't want to wait for me to fix it,
just do it yourself! The code is right here, just submit a pull request, and if it's a
good suggestion I'll be  sure to merge it in.
