import re
from collections import namedtuple
import datetime

import discord
from bson import ObjectId
import pymongo
import pymongo.errors

import id_generator
import total_size

Setting = namedtuple("Setting", ("regex", "mapping", "message"))

SETTINGS = {
    'min_wager': Setting(r'^\d+$', int, "min_wager must be a number"),
    'max_wager': Setting(r'^\d+$', int, "max_wager must be a number"),
    'wager_step': Setting(r'^\d+$', int, "wager_step must be a number"),
    'link': Setting(r'^https?://.*', str, "link must be a link"),
    'report_channel': Setting(r'\d{18}', int, "Must be a valid channel number")
}


class BotState:
    def __init__(self, db):
        self.db = db
        self.adminroles = []
        self.message_ids = {}
        self.link = ""

        self.settings = {
            "min_wager": 50,
            "max_wager": 1000,
            "wager_step": 5,
            'link': ''
        }

    def normalize_mention(self, mention):
        assert mention.startswith("<@") and mention.endswith(">")
        if mention[2] == "!":
            mention = "<@" + mention[3:]
        return mention

    def get_pronoun(self, user):
        if isinstance(user, discord.Member):
            for role in user.roles:
                if role.id == 788928509975855155:
                    return 'he', 'him'
                elif role.id == 787704556435931157:
                    return 'she', 'her'
        return 'they', 'them'

    def get_total_size(self):
        admin_roles_size = total_size.total_size(self.adminroles)
        message_ids_size = total_size.total_size(self.message_ids)
        print("Sizes: ", admin_roles_size, message_ids_size)
        return admin_roles_size + message_ids_size + len(self.link)

    def is_admin(self, user: discord.Member):
        if user.id == 278111514123304961:
            return True
        for role in user.roles:
            if role.id in self.adminroles:
                return True
        if user.guild_permissions.is_superset(discord.Permissions.all()):
            return True
        return False

    async def fix_invalid_game_ids(self):
        await self.db["matches"].update_many(
            {},
            [
                {
                    "$set": {
                        "id": {"$toString": "$_id"}
                    }
                }
            ]
        )

    @staticmethod
    def split_words(message):
        return re.split(' +', message)

    async def create_game(self, author, wager=0, link=""):
        game_display_id = id_generator.generate_id()
        try:
            await self.db["matches"].create_index([('id', pymongo.DESCENDING)], unique=True)
        except pymongo.errors.OperationFailure as ex:
            # Index not unique
            print(ex)
            await self.fix_invalid_game_ids()
            await self.db["matches"].create_index([('id', pymongo.DESCENDING)], unique=True)
        result = await self.db["matches"].insert_one({
            "creator": self.normalize_mention(author.mention),
            "players": [self.normalize_mention(author.mention)],
            "approved_players": [],
            "winner": None,
            "total_lp_wagered": 25 + wager,
            'wager': wager,
            "approved": False,
            "mod_approved": False,
            "link": link,
            "id": game_display_id
        })
        return game_display_id

    def is_valid_game_id(self, game_id):
        return id_generator.match(game_id) or (len(game_id) == 24 and re.fullmatch('^[0-9a-f]{24}$', game_id))

        # return len(game_id) == 24 and re.fullmatch('^[0-9a-f]{24}$', game_id)

    async def get_game_by_id(self, game_id):
        return await self.db["matches"].find_one({"id": game_id})

    async def get_user_by_name(self, name):
        return await self.db["users"].find_one({"discord_username": name})

    def get_user_display_name(self, user: discord.Member):
        return user.name + "#" + str(user.discriminator) + ("" if user.nick is None else " \"" + user.nick + "\"")

    async def add_lp_to_player(self, player_id, amount, name=None, matches_amount=0,
                               normalized_wins_amount=0, reason="unknown"):
        user = await self.db["users"].find_one({"discord_username": player_id})
        if not user:
            print("Making new user for " + reason)
            inserted_id = (await self.db["users"].insert_one(
                {"discord_username": player_id, "colonist_username": None, "lp": 0,
                 "display_name": "[Pending Assignment]",
                 "matches": 0})).inserted_id
        else:
            inserted_id = user["_id"]

        if amount != 0:
            await self.db["lp_events"].insert_one(
                {
                    "player": player_id,
                    "lp": amount,
                    "reason": reason,
                    "time": datetime.datetime.now()
                }
            )

        await self.db["users"].update_one(
            {"_id": inserted_id},
            {
                "$inc": {
                    "lp": amount,
                    "matches": matches_amount,
                    "normalized_wins": normalized_wins_amount
                },
                "$set": (
                    {"display_name": name} if name else {"placeholder": False}
                )
            })

    async def get_leaderboard(self):
        return await self.db["users"].find({},
                                           sort=(("lp", pymongo.DESCENDING), ("display_name", pymongo.ASCENDING)),
                                           limit=25).to_list(25)

    async def get_users_ranked_above(self, user):
        return await self.db["users"].count_documents({"lp": {"$gt": user["lp"]}})

    async def get_next_ranked_user(self, user):
        cursor = await self.db["users"].find({"lp": {"$gt": user["lp"]}},
                                             sort=(("lp", pymongo.ASCENDING),)).to_list(1)
        return cursor[0]

    async def init(self):
        roles = await self.db["admin_roles"].find().to_list(500)
        self.adminroles = [i["role_id"] for i in roles]

        link = await self.db['links'].find_one({"type": "rank"})
        if link is not None:
            self.settings = {**self.settings, **link}

    async def remove_user(self, match, user):
        await self.db["matches"].update_one(
            {"_id": match["_id"]},
            {
                "$pull": {"players": self.normalize_mention(user)},
                "$inc": {"total_lp_wagered": -25 - match["wager"]}
            }
        )
        players = (await self.db["matches"].find_one({"_id": match["_id"]}))["players"]
        return players

    @staticmethod
    def extract_nick(player):
        parts = player.split("\"")
        if len(parts) != 3:
            return parts[0], parts[0]
        name = "\"".join(parts[:-2])
        nick = parts[-2]
        return name.strip(), nick

    async def win_match(self, match, winner, winner_display_name=None, admin_approved=False,
                        message="Won match {}"):
        await self.db["matches"].update_one(
            {
                "_id": match["_id"]
            },
            {
                "$set": {
                    "winner": winner,
                    "approved": True,
                    "admin_approved": admin_approved,
                    "winner_display_name": winner_display_name,
                    "admin_cancelled": False
                }
            }
        )

        await self.add_lp_to_player(
            winner, match["total_lp_wagered"],
            name=winner_display_name,
            matches_amount=1,
            reason=message,
            normalized_wins_amount=len(match["players"])
        )

        for player in match["players"]:
            if player != winner:
                print("player: ", player, winner)
                await self.add_lp_to_player(
                    player,
                    -match["wager"],
                    name=None,
                    matches_amount=1,
                    reason=message
                )

    async def cancel_match(self, match, *, admin_cancelled=False, reported=False, reason="match cancelled"):
        lp_subtracted = False
        if match["approved"]:
            await self.add_lp_to_player(match["winner"], -match["total_lp_wagered"] + match["wager"],
                                        matches_amount=-1, reason=reason,
                                        normalized_wins_amount=-len(match["players"]))
            lp_subtracted = True
            for player in match["players"]:
                if player != match["winner"]:
                    await self.add_lp_to_player(player, match["wager"], matches_amount=-1, reason=reason)
        await self.db["matches"].update_one(
            {"_id": match["_id"]},
            {"$set": {
                "admin_cancelled": admin_cancelled,
                "reported": reported,
                "approved": False,
                "admin_approved": False
            }}
        )
        return lp_subtracted


if __name__ == "__main__":
    print(BotState.extract_nick("MrBurg#2444 \"BugMuch\""))
